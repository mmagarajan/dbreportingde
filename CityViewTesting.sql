 DROP TABLE IF exists #new;
 DROP TABLE IF exists #old;
 DROP TABLE IF EXISTS #oldcities;
 SELECT * INTO #new FROM [dbo].[VW_TDE_CityView_Initiative_Locations_All_Cities]  WHERE source_type='Strategic Initiatives';
 SELECT  [Focus Area], city, state, country, Initiatives, Year_Range, initiative_description INTO #old FROM dbo.VW_FLX_CV_Payment_Locations WHERE Source='Strategic Initiative Locations'  ;
 SELECT DISTINCT city,state, country INTO #oldcities FROM dbo.VW_FLX_CV_Payment_Locations WHERE NULLIF(City,'') IS NOT null
 
 SELECT * FROM #new WHERE country_name='Nigeria' ORDER BY initiative_type
 ;

 WITH new AS (
 SELECT 
 focus_area
 , CASE WHEN initiative_type ='City' THEN city_name ELSE '' END city
 , CASE WHEN initiative_type IN ('City','State') AND country_name='United States' THEN state_name ELSE '' END state
 , country_name
 , initiative_name
 , year_range
 , initiative_description 
 FROM 
 #new  
 GROUP BY 
 focus_area
 , CASE WHEN initiative_type ='City' THEN city_name ELSE '' END  
 , CASE WHEN initiative_type IN ('City','State') AND country_name='United States' THEN state_name ELSE '' END  
 , country_name
 , initiative_name
 , year_range
 , initiative_description 
 )
 , old AS 
 (
  SELECT [Focus Area], City, CASE WHEN Country='United States' THEN State ELSE '' END state , Country, Initiatives, Year_Range, initiative_description FROM #old --WHERE City='Glasgow'
)

SELECT 
COALESCE(a.city, b.City) city
, COALESCE(a.state, b.state) state
, COALESCE(a.country_name, b.Country) country
, a.initiative_name new_initiative
, a.year_range new_year_range
, b.Initiatives old_initiative
, b.Year_Range old_year_range
, oc.City
FROM new  a
FULL OUTER JOIN old b
ON a.city = b.City AND a.state = b.state AND a.country_name = b.Country AND a.initiative_name = b.Initiatives
LEFT JOIN #oldcities oc ON oc.Country = b.Country  AND oc.State = b.state
WHERE (a.city IS NULL AND oc.City IS NOT NULL) OR b.City IS null
ORDER BY 
 a.initiative_name  
, a.year_range  
, b.Initiatives  
, b.Year_Range   